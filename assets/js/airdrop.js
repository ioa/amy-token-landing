
$('#startAirdrop').click(async function (event) {
  event.preventDefault();
  $('#transaction-status').show().text('Admin transaction submitted, confirm in your wallet.')

  AMYAirdrop.methods.finilize()
      .send({ from: userAccount })
      .on('transactionHash', function (hash) {
          $('#transaction-status').html('Starting Airdrop is being processed... <br />Transaction Hash: ' + getTransactionUrl(hash));
        })
      .on('confirmation', function(confNumber, receipt){
        console.log("confirmation");
        console.log(confNumber);
        console.log(receipt);
      })
      .on('error', function (error) {
          $('#transaction-status').html('There was an error starting Airdrop.<br />' + String(error.reason));
          console.log(error.reason);
          throw error;
        })
})

$('#finilizeAirdrop').click(async function (event) {
  event.preventDefault();
  $('#transaction-status').show().text('Admin transaction submitted, confirm in your wallet.')

  AMYAirdrop.methods.finilize()
      .send({ from: userAccount })
      .on('transactionHash', function (hash) {
          $('#transaction-status').html('Finilizing Airdrop is being processed... <br />Transaction Hash: ' + getTransactionUrl(hash));
        })
      .on('confirmation', function(confNumber, receipt){
        console.log("confirmation");
        console.log(confNumber);
        console.log(receipt);
      })
      .on('error', function (error) {
          $('#transaction-status').html('There was an error finilizing Airdrop.<br />' + String(error.reason));
          console.log(error.reason);
          throw error;
        })
})

$('#changeAirdropTime').click(async function (event) {
  event.preventDefault();
  $('#transaction-status').show().text('Admin transaction submitted, confirm in your wallet.')
  var timeStamp = new Date($('#setAirdropTime').val()).getTime()/1000| 0;
    AMYAirdrop.methods.setAirdropTime( timeStamp )
      .send({ from: userAccount })
      .on('error', function (error) {
          $('#transaction-status').html('Error on AirdropTime change.<br />' + String(error.reason));
          console.log(error.reason);
          throw error;
        })
})


async function checkAccountDetail() {
  if (!userAccount) {
    onConnection(false);
    return
  }

  $('#connected-wallet').text(userAccount);

  ///////////////////////////////////////////////////////////////
  // WALLET DATA
  ///////////////////////////////////////////////////////////////

  // Token balance
  AMYToken.methods.balanceOf(userAccount).call().then(function(res){
    $('#amy-amount').text( currencyAMY(res) );
  });

  AMYToken.methods.allowance(tokenWalletAddress , userAccount).call().then(function(res){
    $('#amy-allowance').text( currencyAMY(res) );
  });

  AMYAirdrop.methods.isAdmin(userAccount).call().then(function(res){
    if(res){
        $('#isAirdropAdmin').text( '* ADMIN *' );
    }else{
        $('#isAirdropAdmin').text( ' not admin ' );
    }
  })

  ///////////////////////////////////////////////////////////////
  // VESTING DATA
  ///////////////////////////////////////////////////////////////

  AMYAirdrop.methods.amyAirdropTime().call().then(function(res){
    var listingTime = new Date( Number(res) * 1000 );
    $('#airdrop-time').text( dateFormat.format( airdropTime ));
    countdown('#airdrop-period', airdropTime);
  });

  AMYToken.methods.balanceOf(airdropAddress).call().then(function(res){
    $('#airdrop-balance').text( currencyAMY(res) );
  })


}
