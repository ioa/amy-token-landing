var USDCToken;   //loaded from js script file AMYToken.js
var AMYToken;   //loaded from js script file AMYToken.js
var AMYSale;//loaded from js script file AMYSale.js
var AMYSeedSale;//loaded from js script file AMYSeedSale.js
var AMYVesting;//loaded from js script file AMYVesting.js
var rate;
var netId;
var web3;
var userAccount = false;
var purchaseTime;
var listingTime;
var cliffTime;
var durationTime;


function stringify(x) {
  return Object.prototype.toString.call(x);
}

////////////////////////// Currency Formatting
var formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
});

function weiAMY(amount) {
  return Web3.utils.fromWei(amount); // DO NOT CHANGE! we use (mwei / 10) here because AMY has 18 decimals!
}
function currencyAMY(amount) {
  return formatter.format(weiAMY(amount)).replace(/(^\$)/g, "₳");
}
function weiUSDC(amount) {
  return Web3.utils.fromWei(amount, 'mwei');// DO NOT MODIFY, must use mwei because USDC has 6 decimanls
}
function currencyUSDC(amount) {
  return formatter.format(weiUSDC(amount));
}

////////////////////////// Date Formatting
var dateOptions = { dateStyle: 'medium', timeStyle: 'full' };
var dateFormat = new Intl.DateTimeFormat('en-GB', dateOptions);

function countdown(elem, targetDate) {

  const timer = setInterval(() => {
    // Get today's date and time
    var now = new Date().getTime();
    // Find the distance between now and the count down date
    var distance = targetDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    $(elem).html("(" + days + "d " + hours + "h " + minutes + "m " + seconds + "s)");
    if (distance < 0) {
      $(elem).remove();
      clearInterval(timer);
    }
  }, 1000);

}

const showToast = (type, title, message) => {
  const isMobile = window.innerWidth < 760;
  $.toast({
    heading: title,
    text: message,
    icon: type,
    hideAfter: 7000,
    position: isMobile ? 'bottom-center' : 'top-center',
    bgColor: type === 'success' ? '#53a653' : '#dc3545'
  })
}


///////////////////////////////////////////
// WEB3 and Wallet Provider
//////////////////////////////////////////
const Web3Modal = window.Web3Modal.default;
const WalletConnectProvider = window.WalletConnectProvider.default;
const Fortmatic = window.Fortmatic;
const evmChains = window.evmChains;

// Web3modal instance
let web3Modal

// Chosen wallet provider given by the dialog window
let provider;


// Address of the selected account
let selectedAccount;


// Checking if Web3 has been injected by the browser (Mist/MetaMask)
window.addEventListener('load', function () {

  // Tell Web3modal what providers we have available.
  // Built-in web browser provider (only one can exist as a time)
  // like MetaMask, Brave or Opera is added automatically by Web3modal
  const providerOptions = {
    walletconnect: {
      package: WalletConnectProvider,
      options: {
        // Mikko's test key - don't copy as your mileage may vary
        infuraId: "d1a4de75573a493d9a55f27b4e0dd3a2",
        rpc: {
          137: "https://rpc.matic.today",
          80001: "https://matic-mumbai.chainstacklabs.com",
          // ...
        },
      }
    }

    // fortmatic: {
    //   package: Fortmatic,
    //   options: {
    //     // Mikko's TESTNET api key
    //     key: "pk_test_391E26A3B43A3350"
    //   }
    //}
  };

  web3Modal = new Web3Modal({
    cacheProvider: false, // optional
    providerOptions, // required
    disableInjectedProvider: false, // optional. For MetaMask / Brave / Opera.
  });

})

async function connect() {

  try {
    provider = await web3Modal.connect();
    await fetchAccountData();
    if (!userAccount) {
      onConnection(false);
    }
  } catch (e) {
    console.log("Could not get a wallet connection", e);
    return;
  }

  // Subscribe to accounts change
  provider.on("accountsChanged", (accounts) => {
    fetchAccountData();
  });

  // Subscribe to chainId change
  provider.on("chainChanged", (chainId) => {
    fetchAccountData();
  });

  provider.on("connect", (info) => {
    fetchAccountData();
  });

  provider.on("disconnect", (error) => {
    console.error(error);
    onConnection(false);
  });
};

/**
 * Kick in the UI action after Web3modal dialog has chosen a provider
 */
async function fetchAccountData() {

  // Get a Web3 instance for the wallet
  web3 = new Web3(provider);

  // Get list of accounts of the connected wallet
  const chainId = await web3.eth.net.getId()

  if (chainId !== netId) {
    showToast('error', 'Wrong network', 'Please connect your wallet to the Polygon network');
    return;
  }
  const accounts = await web3.eth.getAccounts();

  // Setting handleRevert to true to obtain the custom revert messages from Solidity contracts.
  web3.eth.handleRevert = true;

  userAccount = accounts[0];

  walletConnected();
}

function initForNetwork(chainId) {    // Check if in mainnet, testnets or local

  let local_provider = '';

  netId = chainId;
  // >>>>>>> Polygon Mainnet <<<<<<<<<
  if (chainId === 137) {

    local_provider = 'https://rpc-mumbai.matic.today';

    usdcAddress = usdcMainnet;
    amyAddress = amyMainnet;
    saleAddress = saleMainnet;
    vestingAddress = vestingMainnet;
  }
  // >>>>>>> Polygon Mumbai Testnet <<<<<<<<<
  else if (chainId === 80001) {

    local_provider = 'https://matic-mumbai.chainstacklabs.com';

    usdcAddress = usdcTestnet;
    amyAddress = amyTestnet;
    saleAddress = saleTestnet;
    vestingAddress = vestingTestnet;
  }
  // >>>>>>> Local Ganache Testnet <<<<<<<<<
  else if (chainId === 1337) {

    local_provider = 'http://127.0.0.1:7545';

    usdcAddress = usdcLocalnet;
    amyAddress = amyLocalnet;
    saleAddress = saleLocalnet;
    vestingAddress = vestingLocalnet;
  }
  else { // No network is found.
    alert('Wrong network')
  }

  var web3Provider = new Web3.providers.HttpProvider(local_provider);
  var localWeb3 = new Web3(web3Provider);

  // Get hold of the ABI describing the ABI of the contracts.
  // these are loaded from the JS files abiERC20 is inside of abiERC20.js, etc.
  // Get hold of contract instance
  USDCToken = new localWeb3.eth.Contract(abiERC20, usdcAddress);
  AMYToken = new localWeb3.eth.Contract(abiERC20, amyAddress);
  AMYSale = new localWeb3.eth.Contract(abiAMYSale, saleAddress);
  AMYVesting = new localWeb3.eth.Contract(abiAMYVesting, vestingAddress);

  insertAddress(amyAddress, 'token', 'token-address', true);
  insertAddress(saleAddress, 'address', 'sale-address', true);
  insertAddress(vestingAddress, 'address', 'vesting-address', true);
}

const insertAddress = (address, type, targetContainer, full) => {

  const link = document.createElement('a');
  const div = document.createElement('div');
  const span = document.createElement('span');
  const img = document.createElement('img');
  link.setAttribute('class', 'address');
  link.setAttribute('target', '_blank');
  link.setAttribute('href', getExplorerUrl(type, address));
  img.setAttribute('src', 'assets/img/maximize.svg')

  const trimmedAddress = full ? address : address.substr(0, 6) + "..." + address.substr(address.length - 4);

  span.appendChild(document.createTextNode(trimmedAddress));
  div.appendChild(span);
  div.appendChild(img);
  link.appendChild(div);

  document.getElementById(targetContainer).replaceWith(link);

}
function walletConnected() {
  onConnection(true);
  insertAddress(userAccount, 'address', 'connected-wallet', true);

  // Update account detail every 3 seconds
  setInterval(checkAccountDetail, 3000);
  checkAccountDetail()
  enablePurchase();
}

function onConnection(connected) {
  $('#not-connected').toggle(!connected);
  $('#connected').toggle(connected);
  $('#buy-button').prop('disabled', !connected);
  $('#release-button').prop('disabled', !connected);
}

function getTransactionUrl(address) {
  return getExplorerUrl('tx', address);
}

function getTokenUrl(address) {
  return getExplorerUrl('token', address);
}

function getContractUrl(address) {
  return getExplorerUrl('address', address);
}

function getExplorerUrl(type, address) {
  var url = '';

  if (netId === 137) { //Mainnet
    url = 'polygonscan.com';
  }
  else if (netId === 80001) { //Testnet
    url = 'mumbai.polygonscan.com';
  }
  else if (netId === 1337) { //Local
    url = 'ganache';
  }
  else { alert('errors scan'); }

  return "https://" + url + '/' + type + '/' + address;
}

const setLoading = (isLoading) => {
  isLoading ? $('#loader').show() : $('#loader').hide();
}