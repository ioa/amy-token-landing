////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
// LOCAL // Localnet on Ganache, these need to reflect your local Ganache addresses, you must copy and paste them here.
////////////////////////////////////////////////////////////////////
var amyLocalnet = '0xaFb59e13c24aCe911934cB68a6F355e07b05b6F4';// <<<< Cut and Paste from Truffle Migration the AMY Token address
var saleLocalnet = '0x6aa8b25842B5F0cE508f80e10150De5D763D050D';// <<<< Cut and Paste from Truffle Migration the AMY Sale address
var vestingLocalnet = '0x191D7C15807a16c0F25e1801Ce7256D36475C271';// <<<< Cut and Paste from Truffle Migration the AMY Vesting address
var airdropLocalnet = '0xc57b48fb98c59e2a9CE3cD1d07D55573298eac6c';// <<<< Cut and Paste from Truffle Migration the AMY Airdrop address
var usdcLocalnet = '0x66FB7B884B841eDc544D6434563f61968C09664e';// <<<< Cut and Paste from Truffle Migration the USDC Test Token address

var tokenWalletLocalnet = '0x71647Ab44704077583BCEcD42575b9b2a7607872';// 1st wallet address from ganache.
var saleWalletLocalnet = '0xC6D98b10238739d472cf185976212d8801ce2380';// 2ND
var altAdminLocalnet = '0x457f2291324852f3963f0A99cF88A51C42A87994'; // 4th
////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////
// MUMBAI TESTNET  // Copy & Pasted from Testnet deployment output or Remix addresses after deployment
////////////////////////////////////////////////////////////////////
var amyTestnet = '0x984b13A9865Af52DD2d82Ff80720726b0b3ebFF5';
var saleTestnet = '0xa43FC8914Ef296353EBCE41c34BC5751f65Ce739';
var vestingTestnet = '0xEd99DF74C17e8971cA266e355C0F143BbB609Be4';
var airdropTestnet = '0x643707E13a4955Ee6b418F4B4f3E6d4FB87f3FF8';
var usdcTestnet = '0xf1dd34936Bc1C13aC312eF3dE88f559504D6262d'; //Token Label: new AMY USDC Test Token

var tokenWalletTestnet = '0x71647Ab44704077583BCEcD42575b9b2a7607872';// Steve Admin
var saleWalletTestnet = '0xC6D98b10238739d472cf185976212d8801ce2380';// Steve 2nd
var altAdminTestnet = '0x457f2291324852f3963f0A99cF88A51C42A87994';// Ramon Admin
////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////
// POLYGON MAINNET // Copy & Pasted from Mainnet deployment output or Remix addresses after deployment
////////////////////////////////////////////////////////////////////
var amyMainnet = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx';//
var saleMainnet = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx';//
var vestingMainnet = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx';//
var airdropMainnet = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx';
var usdcMainnet = '0x2791bca1f2de4661ed88a30c99a7a9449aa84174';// DO NOT CHANGE THIS.

var tokenWalletMainnet = '0x71647Ab44704077583BCEcD42575b9b2a7607872';// Steve Admin
var saleWalletMainnet = '0xC6D98b10238739d472cf185976212d8801ce2380';// Steve 2nd
var altAdminMainnet = '0x457f2291324852f3963f0A99cF88A51C42A87994'; // Ramon Admin
////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////
// GLOBAL VARIABLES
////////////////////////////////////////////////////////////////////
var saleAddress = saleTestnet;
var amyAddress = amyTestnet;
var altAdmin = altAdminTestnet
var vestingAddress = vestingTestnet;
var airdropAddress = airdropTestnet;
var usdcAddress = usdcTestnet;

var tokenWalletAddress = tokenWalletTestnet;
var saleWalletAddress = saleWalletTestnet;
var altAdminAddress = altAdminTestnet;
////////////////////////////////////////////////////////////////////