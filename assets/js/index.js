

"use strict";

function allocationChart() {
	var data = {
		labels: [
			'Seed Sale',
			'Private Sale',
			'Public Sale IDO',
			'Community Rewards',
			'Team',
			'Angels',
			'Advisors',
			'Reserve'
		],
		datasets: [{
			label: 'AMY Token Allocation',
			data: [6, 5, 4, 30, 20, 10, 10, 3, 2],
			hoverOffset: 4,
			borderWidth: 1,
			backgroundColor: [
				'rgba(255, 99, 132, 0.6)',
				'rgba(54, 162, 235, 0.6)',
				'rgba(255, 206, 86, 0.6)',
				'rgba(75, 192, 192, 0.6)',
				'rgba(153, 102, 255, 0.6)',
				'rgba(153, 159, 102, 0.6)',
				'rgba(255, 200, 89, 0.6)',
				'rgba(54, 255, 150, 0.6)',
				'rgba(150, 206, 160, 0.6)',
				'rgba(104, 240, 192, 0.6)',
				'rgba(104, 240, 192, 0.6)',
				'rgba(153, 190, 75, 0.6)'
			],
		}]
	};

	var config = {
		type: 'pie',
		data: data,
		options: {
			responsive: true,
			plugins: {
				legend: {
					position: 'left',
					display: false
				}
			}
		}
	};

	var ctx = document.getElementById('allocationChart');
	var myChart = new Chart(ctx, config);
}

const journeyList = document.querySelectorAll("#journey ul")[0];

window.setInterval(() => {
	const position = journeyList.getBoundingClientRect();

	if ((position.top >= 0 && position.bottom <= window.innerHeight) || (position.top < window.innerHeight && position.bottom >= 0)) {
		journeyList.scrollLeft = journeyList.scrollLeft + 1;
	}
}, 20);


const contactUs = (e) => {
	e.preventDefault();

	const d = document;

	const form = d.querySelector("#contact-form");

	Array.from(form.elements).forEach(formElement => formElement.disabled = true);

	const name = d.querySelector('#name').value;
	const email = d.querySelector('#email').value;
	const message = d.querySelector('#message').value;

	fetch(`https://api.amy.network/rest/open/tokenContact`, {
		body: JSON.stringify({
			name, email, message
		}),
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Accept': 'application/vnd.fleeber.media-v1+json;charset=utf-8'
		}
	}).then(response => {
		alert("Your message has been sent. Thank you.")
		Array.from(form.elements).forEach(formElement => formElement.disabled = false);
	}).catch(err => {
		Array.from(form.elements).forEach(formElement => formElement.disabled = false);
	});
}

// count down timer
const countDownClock = (year, month, day, hours) => {

	const d = document;
	const daysElement = d.querySelector('.days');
	const hoursElement = d.querySelector('.hours');
	const minutesElement = d.querySelector('.minutes');
	const secondsElement = d.querySelector('.seconds');
	let countdown;

	let future = new Date(year, month, day, hours, 0, 0);

	countdown = setInterval(() => {
		const secondsLeft = Math.round((future.getTime() - new Date().getTime()) / 1000);

		if (secondsLeft <= 0) {
			clearInterval(countdown);
			return;
		};

		displayTimeLeft(secondsLeft);

	}, 1000);


	function displayTimeLeft(seconds) {
		daysElement.textContent = Math.floor(seconds / 86400);
		hoursElement.textContent = Math.floor((seconds % 86400) / 3600);
		minutesElement.textContent = Math.floor((seconds % 86400) % 3600 / 60);
		secondsElement.textContent = seconds % 60 < 10 ? `0${seconds % 60}` : seconds % 60;
	}
}


/*
	start countdown
	enter number and format
	days, hours, minutes or seconds
*/
countDownClock(2021, 9, 1, 18);
allocationChart();
