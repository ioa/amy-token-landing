/////////////////////////////////////// SALE
// Functions for buying with USD
$('#buy_amount').change(function () {
  var totalAMY = (this.value * rate).toFixed(4);
  totalAMY = parseFloat(totalAMY);
  $('#totalAMY').val(totalAMY);
})

$('#buy_amount').keyup(function () {
  var totalAMY = (this.value * rate).toFixed(4);
  totalAMY = parseFloat(totalAMY);
  $('#totalAMY').val(totalAMY);
})

$('#buy-button').click(async function (event) {
  event.preventDefault()
  $('#sale-status').show().text('Sale transaction submitted, confirm in your wallet.')

  var amount = parseInt($('#buy_amount').val())
  var amountWei = web3.utils.toWei(amount.toFixed(4), 'mwei');// DO NOT CHANGE! we use mwei here because USDC has 6 decimals!

  // Require user approval
  await USDCToken.methods.approve(saleAddress, amountWei).send({ from: userAccount });

  AMYSale.methods.buyVesting(amountWei)
    .send({ from: userAccount })
    .on('transactionHash', function (hash) {
      $('#sale-status').html('Your contribution is being processed. Transaction Hash: <br />' + getTransactionUrl(hash));
    })
    .on('receipt', function (receipt) {
      $('#sale-status').html('Congratulations! Your your AMY tokens are now deposited in the Vesting Contract!' + String(receipt));
    })
    .on('confirmation', function (confNumber, receipt) {
      console.log('confirmation: ' + String(confNumber) + ' and Receipts: ' + String(receipt));
    })
    .on('error', function (error) {
      $('#sale-status').html('There was an error processing your contribution: <br />' + String(error.reason));
      console.log('Error Thrown: ' + String(error.reason));
      throw error;
    })
})


/////////////////////////////////////// VESTING
// Functions for withdrawing AMYs from the vesting contract
$('#release-button').click(async function (event) {
  event.preventDefault()
  $('#vesting-status').show().text('Withdraw submitted, please confirm in MetaMask.')

  await AMYVesting.methods.release()
    .send({ from: userAccount })
    .on('transactionHash', function (hash) {
      $('#vesting-status').html('Your withdraw is being processed. Transaction Hash: <br /> ' + getTransactionUrl(hash));
      console.log('Transaction Url: ' + getTransactionUrl(hash));
    })
    .on('receipt', function (receipt) {
      $('#vesting-status').html('Congratulations! Your withdraw has been processed and the beneficiary received AMY Tokens!');
      console.log('receipt: ' + receipt);
    })
    .on('confirmation', function (confNumber, receipt) {
      $('#vesting-status').html('Congratulations! Your withdraw has been processed and the beneficiary received AMY Tokens!');
      console.log('confNumber: ' + confNumber);
      console.log('receipt: ' + receipt);
    })
    .on('error', function (error) {
      $('#vesting-status').html('There was an error processing your withdraw: <br />' + String(error.reason));
      console.log(String(error.reason));
      throw error;
    })
})

/////////////////////////////////////
//////////////////////////// CONTROLS
$('#changeListingTime').click(async function (event) {
  event.preventDefault();
  $('#transaction-status').show().text('Admin transaction submitted, confirm in your wallet.')
  var timeStamp = new Date($('#setListingTime').val()).getTime() / 1000 | 0;
  AMYVesting.methods.setListingTime(timeStamp)
    .send({ from: userAccount })
    .on('error', function (error) {
      $('#transaction-status').html('Error on ListingTime change.<br />' + String(error.reason));
      console.log(error.reason);
      throw error;
    })
})
$('#changeCliff').click(async function (event) {
  event.preventDefault();
  $('#transaction-status').show().text('Admin transaction submitted, confirm in your wallet.')
  var valueBN = web3.utils.toBN($('#setCliff').val());
  AMYVesting.methods.setCliff(valueBN)
    .send({ from: userAccount })
    .on('error', function (error) {
      $('#transaction-status').html('Error of Cliff change.<br />' + String(error.reason));
      console.log(error.reason);
      throw error;
    })
})
$('#changeDuration').click(async function (event) {
  event.preventDefault();
  $('#transaction-status').show().text('Admin transaction submitted, confirm in your wallet.')
  var valueBN = web3.utils.toBN($('#setDuration').val());
  AMYVesting.methods.setDuration(valueBN)
    .send({ from: userAccount })
    .on('error', function (error) {
      $('#transaction-status').html('Error on Duration change.<br />' + String(error.reason));
      console.log(error.reason);
      throw error;
    })
})

$('#changeStage').click(async function (event) {
  event.preventDefault();
  $('#transaction-status').show().text('Admin transaction submitted, confirm in your wallet.')
  var value = web3.utils.toBN($('#setStage').val());
  AMYSale.methods.setStage(value)
    .send({ from: userAccount })
    .on('error', function (error) {
      $('#transaction-status').html('Error on setStage.<br />' + String(error.reason));
      console.log(error.reason);
      throw error;
    })
})


$('#initiateVesting').click(async function (event) {
  event.preventDefault();
  $('#transaction-status').show().text('Admin transaction submitted, confirm in your wallet.')

  var totalSupply;
  await AMYToken.methods.totalSupply().call().then(function (res) { totalSupply = res; });
  var internalAllocation = (totalSupply * 15) / 100; // (Team allocation of 10.00%) + (Angel allocation of 5%) = 15%
  var amountBN = web3.utils.toBN(internalAllocation);

  // Require OWNER/ADMIN approval
  await AMYToken.methods.approve(vestingAddress, amountBN).send({ from: userAccount });

  AMYVesting.methods.initializeInternalVesting(amountBN)
    .send({ from: userAccount })
    .on('transactionHash', function (hash) {
      $('#transaction-status').html('Your deposit initiation is being processed... <br />Transaction Hash: ' + getTransactionUrl(hash));
    })
    .on('confirmation', function (confNumber, receipt) {
      console.log("confirmation");
      console.log(confNumber);
      console.log(receipt);
    })
    .on('error', function (error) {
      $('#transaction-status').html('There was an error processing the deposit initiation.<br />' + String(error.reason));
      console.log(error.reason);
      throw error;
    })
})

$('#internalDeposit').click(async function (event) {
  event.preventDefault();
  $('#transaction-status').show().text('Admin transaction submitted, confirm in your wallet.')
  var beneficiary = $('#deposit-address').val();
  var amount = parseInt($('#deposit-amount').val());
  var amountWei = web3.utils.toWei(amount.toFixed(4), 'ether');

  await AMYToken.methods.approve(vestingAddress, amountWei).send({ from: userAccount });

  AMYVesting.methods.internalDeposit(beneficiary, amountWei)
    .send({ from: userAccount })
    .on('transactionHash', function (hash) {
      $('#transaction-status').html('Your deposit is being processed... <br />Transaction Hash: ' + getTransactionUrl(hash));
    })
    .on('confirmation', function (confNumber, receipt) {
      console.log("confirmation");
      console.log(confNumber);
      console.log(receipt);
    })
    .on('error', function (error) {
      $('#transaction-status').html('There was an error processing your deposit.<br />' + String(error.reason));
      console.log(error.reason);
      throw error;
    })
})

$('#revokeDeposit').click(async function (event) {
  event.preventDefault();
  $('#transaction-status').show().text('Admin transaction submitted, confirm in your wallet.')
  var beneficiary = $('#revoke-address').val();

  AMYVesting.methods.revokeTokens(beneficiary)
    .send({ from: userAccount })
    .on('transactionHash', function (hash) {
      $('#transaction-status').html('Your revoke is being processed... <br />Transaction Hash: ' + getTransactionUrl(hash));
    })
    .on('confirmation', function (confNumber, receipt) {
      console.log("confirmation");
      console.log(confNumber);
      console.log(receipt);
    })
    .on('error', function (error) {
      $('#transaction-status').html('There was an error revoke.<br />' + String(error.reason));
      console.log(error.reason);
      throw error;
    })
})


$('#finilizeSale').click(async function (event) {
  event.preventDefault();
  $('#transaction-status').show().text('Admin transaction submitted, confirm in your wallet.')

  AMYVesting.methods.finilize()
    .send({ from: userAccount })
    .on('transactionHash', function (hash) {
      $('#transaction-status').html('Finalizing Sale is being processed... <br />Transaction Hash: ' + getTransactionUrl(hash));
    })
    .on('confirmation', function (confNumber, receipt) {
      console.log("confirmation");
      console.log(confNumber);
      console.log(receipt);
    })
    .on('error', function (error) {
      $('#transaction-status').html('There was an error finilizing sale.<br />' + String(error.reason));
      console.log(error.reason);
      throw error;
    })
})



async function checkAccountDetail() {
  if (!userAccount) {
    onConnection(false);
    return
  }

  $('#connected-wallet').text(userAccount);

  ///////////////////////////////////////////////////////////////
  // WALLET DATA
  ///////////////////////////////////////////////////////////////
  // Account balance in USDC
  USDCToken.methods.balanceOf(userAccount).call().then(function (res) {
    $('#usdc-amount').text(currencyUSDC(res));
  });

  // Token balance
  AMYToken.methods.balanceOf(userAccount).call().then(function (res) {
    $('#amy-amount').text(currencyAMY(res));
  });

  AMYToken.methods.allowance(tokenWalletAddress, userAccount).call().then(function (res) {
    $('#amy-allowance').text(currencyAMY(res));
  });


  ///////////////////////////////////////////////////////////////
  // SALE DATA
  ///////////////////////////////////////////////////////////////

  AMYSale.methods.getStage().call().then(function (res) {
    if (res == 0) {
      $('#sale-stage').text('Seed Sale');
    } else if (res == 1) {
      $('#sale-stage').text('Private Sale 1');
    } else if (res == 1) {
      $('#sale-stage').text('Private Sale 2');
    } else if (res == 1) {
      $('#sale-stage').text('Private Sale 3');
    } else if (res == 1) {
      $('#sale-stage').text('Ended');
    } else {
      $('#sale-stage').text('Undefined');
    }
  });


  AMYSale.methods.saleSupply().call().then(function (res) {
    $('#sale-supply').text(currencyAMY(res));
  });

  USDCToken.methods.allowance(userAccount, saleAddress).call().then(function (res) {
    $('#sale-allowance').text(currencyUSDC(res));
  });

  AMYSale.methods.getRate().call().then(function (res) {
    rate = res;
    $('#sale-rate').html(' 1 USDC= <b>' + rate + '</b> AMYs');
  });

  AMYSale.methods.getWallet().call().then(function (r) {
    var saleWallet = r;
    USDCToken.methods.balanceOf(saleWallet).call().then(function (res) {
      $('#sale-raised').text(currencyUSDC(res));
    });
  });


  ///////////////////////////////////////////////////////////////
  // VESTING DATA
  ///////////////////////////////////////////////////////////////

  AMYVesting.methods.listingTime().call().then(function (res) {
    var listingTime = new Date(Number(res) * 1000);
    $('#listing-time').text(dateFormat.format(listingTime));
    countdown('#listing-period', listingTime);
  });

  AMYVesting.methods.purchaseTime(userAccount).call().then(function (res) {
    var purchaseTime = new Date(Number(res) * 1000);
    if (res == 0) {
      $('#purchase-time').text('----------');
    } else {
      $('#purchase-time').text(dateFormat.format(purchaseTime));
    }
  });

  AMYVesting.methods.getCliff().call().then(function (res) {
    $('#cliff').html(res);
  });

  AMYVesting.methods.cliffTime(userAccount).call().then(function (res) {
    var cliffTime = new Date(Number(res) * 1000);
    if (res == 0) {
      $('#cliff-time').text('----------');
      $('#cliff-period').text('');
    } else {
      $('#cliff-time').text(dateFormat.format(cliffTime));
      countdown('#cliff-period', cliffTime);
    }
  });

  AMYVesting.methods.getDuration().call().then(function (res) {
    $('#duration').html(res);
  });

  AMYVesting.methods.durationTime(userAccount).call().then(function (res) {
    var durationTime = new Date(Number(res) * 1000);
    if (res == 0) {
      $('#duration-time').text('----------');
      $('#duration-period').text('');
    } else {
      $('#duration-time').text(dateFormat.format(durationTime));
      countdown('#duration-period', durationTime);
    }
  });

  AMYToken.methods.balanceOf(vestingAddress).call().then(function (res) {
    $('#vesting-balance').text(currencyAMY(res));
  })

  AMYToken.methods.allowance(userAccount, vestingAddress).call().then(function (res) {
    $('#vesting-allowance').text(currencyAMY(res));
  });


  AMYVesting.methods.isAdmin(userAccount).call().then(function (res) {
    if (res) {
      $('#isVestingAdmin').text('* ADMIN *');
    } else {
      $('#isVestingAdmin').text(' not admin ');
    }
  });
  AMYVesting.methods.totalPurchased(userAccount).call().then(function (res) {
    $('#total-purchased').text(currencyAMY(res));
  });
  AMYVesting.methods.getVesting(userAccount).call().then(function (res) {
    $('#vesting').text(currencyAMY(res));
  });
  AMYVesting.methods.getVested(userAccount).call().then(function (res) {
    $('#vested').text(currencyAMY(res));
  });
  AMYVesting.methods.getReleased(userAccount).call().then(function (res) {
    $('#released').text(currencyAMY(res));
  });
  AMYVesting.methods.getReleasable(userAccount).call().then(function (res) {
    $('#releaseable').text(currencyAMY(res));
  });
  AMYVesting.methods.internallyVested().call().then(function (res) {
    $('#internallyVesting').text(currencyAMY(res));
  });

}
