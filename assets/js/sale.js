let listed = false;

/////////////////////////////////////// SALE
// Functions for buying with USDC


const convertToAMY = (value) => {
  let totalAMY = (value * rate).toFixed(4);
  $('#totalAMY').val(parseFloat(totalAMY));
}

const convertFromAMY = (value) => {
  let totalUSDC = (value / rate).toFixed(4);
  $('#buy_amount').val(parseFloat(totalUSDC));
}

$('#buy_amount').change(function (e) {
  convertToAMY(e.target.value);
})

$('#buy_amount').keyup(function (e) {
  convertToAMY(e.target.value);
})

$('#totalAMY').change(function (e) {
  convertFromAMY(e.target.value);
})

$('#totalAMY').keyup(function (e) {
  convertFromAMY(e.target.value);
})

const enablePurchase = () => {
  $('#buy_amount').attr('disabled', false);
  $('#totalAMY').attr('disabled', false);
  $('.bg-dark-900').removeClass('disabled');
  $('#buy-button').attr('disabled', false);
};


$('#buy-button').click(async function (event) {

  event.preventDefault();

  WalletAMYSale = new web3.eth.Contract(abiAMYSale, saleAddress);
  WalletUSDCToken = new web3.eth.Contract(abiERC20, usdcAddress);

  $('#sale-status').show().text('Transaction submitted, please confirm in your wallet.')

  var amount = parseFloat($('#buy_amount').val())

  const balance = await WalletUSDCToken.methods.balanceOf(userAccount).call();

  if (balance < amount) {
    showToast('error', 'No balance', 'You don\'t have enough USDC for this purchase');
    return;
  }

  var amountWei = Web3.utils.toWei(amount.toFixed(4), 'mwei');// DO NOT CHANGE! we use mwei here because USDC has 6 decimals!

  const allowed = await WalletUSDCToken.methods.allowance(userAccount, saleAddress).call();

  if (allowed - amountWei < 0) {
    setLoading(true);
    try {
      await WalletUSDCToken.methods.approve(saleAddress, amountWei).send({ from: userAccount });
    } catch (err) {
      setLoading(false);
      return;
    }
  }

  // Require user approval
  setLoading(true);
  WalletAMYSale.methods.buyVesting(amountWei)
    .send({
      from: userAccount
    })
    .on('transactionHash', function (hash) {
      console.log("Transaction created: " + getTransactionUrl(hash));
    })
    .on('receipt', function (receipt) {
      console.log('receipt: ' + JSON.stringify(receipt));
      showToast('success', 'Success', 'Congratulations! Your AMY tokens have been deposited and are already vesting.');
      setLoading(false);
    })
    .on('confirmation', function (confNumber, receipt) {
      console.log('receipt: ' + JSON.stringify(receipt));
    })
    .on('error', function (error) {
      showToast('error', 'Error', 'There was an error processing your contribution: ' + error.message);
      setLoading(false);
      console.error(error);
    })
})

/////////////////////////////////////// VESTING
// Functions for withdrawing AMYs from the vesting contract
$('#release-button').click(async function (event) {
  event.preventDefault()

  WalletAMYVesting = new web3.eth.Contract(abiAMYVesting, vestingAddress);

  $('#vesting-status').show().text('Withdraw submitted, please confirm in MetaMask.')

  const gasLimit = await WalletAMYVesting.methods.release().estimateGas({ from: userAccount });

  setLoading(true);

  WalletAMYVesting.methods.release()
    .send({ from: userAccount, gasLimit: gasLimit * 1.3 }) //setting gas limit to avoid web3 default one blocking transaction
    .on('transactionHash', function (hash) {
      console.log("Transaction created: " + getTransactionUrl(hash));
    })
    .on('receipt', function (receipt) {
      console.log('receipt: ' + JSON.stringify(receipt));
      showToast('success', 'Success', 'Congratulations! Your withdraw has been processed and the AMY tokens have been sent to your wallet.');
      setLoading(false);
    })
    .on('confirmation', function (confNumber, receipt) {
      console.log('receipt: ' + JSON.stringify(receipt));
    })
    .on('error', function (error) {
      showToast('error', 'Error', 'There was an error with your withdraw: ' + error.message);
      setLoading(false);
      console.error(error);
    })
})

async function checkAccountDetail() {
  if (!userAccount) {
    onConnection(false);
    return
  }

  listed && $('#release_container').show();

  AMYVesting.methods.getPurchases(userAccount).call().then(function (purchases) {

    const tbodyRef = document.getElementById('investor_purchases').getElementsByTagName('tbody')[0];
    const new_tbody = document.createElement('tbody');

    let releasable = Web3.utils.toBN(0);

    if (purchases.length == 0) {
      $('#no_purchases').show();
      return;
    }
    $('#no_purchases').hide();

    for (let i = 0; i < purchases.length; i++) {
      const purchase = purchases[i];
      const newRow = new_tbody.insertRow();

      const date = new Date(Number(purchase["purchaseTime"]) * 1000);

      newRow.insertCell().appendChild(document.createTextNode(date.toLocaleDateString() + " " + date.toLocaleTimeString()));
      newRow.insertCell().appendChild(document.createTextNode(currencyAMY(purchase["purchased"])));
      newRow.insertCell().appendChild(document.createTextNode(currencyAMY(purchase["vested"])));
      newRow.insertCell().appendChild(document.createTextNode(currencyAMY(purchase["released"])));
      newRow.insertCell().appendChild(document.createTextNode(currencyAMY(purchase["releasable"])));

      releasable = releasable.add(Web3.utils.toBN(purchase["releasable"]));
    }

    $('#releasable').text(currencyAMY(releasable));
    tbodyRef.parentNode.replaceChild(new_tbody, tbodyRef);

  });

}

function initialize() {

  initForNetwork(80001);

  AMYSale.methods.saleSupply().call().then(function (res) {
    $('#sale-supply').text(currencyAMY(res));
  });

  AMYVesting.methods.listingTime().call().then(function (res) {
    var listingTime = new Date(Number(res) * 1000);

    var now = new Date().getTime();
    var distance = listingTime - now;

    if (distance > 0) {
      $('#listing-time').text(dateFormat.format(listingTime));
      countdown('#listing-period', listingTime);
      $('#release_container').hide();
      $('#purchase_container').show();
      $('#buy_at').hide();
    } else {
      listed = true;
      $('#release_locked').hide();
      $('#buy_at').show();
      $('#purchase_container').hide();
      if (userAccount) {
        $('#release_container').show();
      }
    }
  });

  AMYSale.methods.getStages().call().then(function (res) {
    const tbodyRef = document.getElementById('stages').getElementsByTagName('tbody')[0];
    const new_tbody = document.createElement('tbody');

    let currentActive = 0;
    for (let i = 0; i < res.length; i++) {
      const stage = res[i];

      const newRow = new_tbody.insertRow();

      let name = "";
      currentActive = stage.active ? Number(stage.stage) : currentActive;
      const active = stage.active ? 'Active' : (Number(stage.stage) > currentActive ? 'Not started' : 'Finished');
      stage.active && newRow.setAttribute('class', 'active');
      const rate = stage.props.rate;
      switch (stage.stage) {
        case "0":
          name = 'Seed Sale';
          break;
        case "1":
          name = 'Private Sale 1';
          break;
        case "2":
          name = 'Private Sale 2';
          break;
        default:
          name = 'Private Sale 3';
      }

      newRow.insertCell().appendChild(document.createTextNode(name));
      newRow.insertCell().appendChild(document.createTextNode('1 USDC : ' + rate + ' AMY'));
      newRow.insertCell().appendChild(document.createTextNode(active));

    }

    tbodyRef.parentNode.replaceChild(new_tbody, tbodyRef);

  });

  AMYSale.methods.getRate().call().then(function (res) {
    rate = res;
    $('#sale-rate').html('1 USDC = <b>' + rate + '</b> AMY');
  });

  AMYVesting.methods.totalVestingBalance().call().then(function (res) {
    $('#sold-tokens').text(currencyAMY(res));
  });

}

initialize();