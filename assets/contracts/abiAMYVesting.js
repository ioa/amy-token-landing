abiAMYVesting =
	[
		{
			"inputs": [
				{
					"internalType": "contract AMYToken",
					"name": "amyAddress",
					"type": "address"
				},
				{
					"internalType": "address",
					"name": "saleWallet",
					"type": "address"
				},
				{
					"internalType": "uint256",
					"name": "_listingTime",
					"type": "uint256"
				},
				{
					"internalType": "uint256",
					"name": "vestingDuration",
					"type": "uint256"
				}
			],
			"stateMutability": "nonpayable",
			"type": "constructor"
		},
		{
			"anonymous": false,
			"inputs": [
				{
					"indexed": true,
					"internalType": "address",
					"name": "previousOwner",
					"type": "address"
				},
				{
					"indexed": true,
					"internalType": "address",
					"name": "newOwner",
					"type": "address"
				}
			],
			"name": "OwnershipTransferred",
			"type": "event"
		},
		{
			"anonymous": false,
			"inputs": [
				{
					"indexed": false,
					"internalType": "address",
					"name": "investor",
					"type": "address"
				},
				{
					"indexed": false,
					"internalType": "uint256",
					"name": "amount",
					"type": "uint256"
				}
			],
			"name": "TokenRevoked",
			"type": "event"
		},
		{
			"anonymous": false,
			"inputs": [
				{
					"indexed": false,
					"internalType": "address",
					"name": "token",
					"type": "address"
				}
			],
			"name": "TokenVestingRevoked",
			"type": "event"
		},
		{
			"anonymous": false,
			"inputs": [
				{
					"indexed": false,
					"internalType": "address",
					"name": "sender",
					"type": "address"
				},
				{
					"indexed": false,
					"internalType": "uint256",
					"name": "amount",
					"type": "uint256"
				},
				{
					"indexed": false,
					"internalType": "uint256",
					"name": "purchaseTime",
					"type": "uint256"
				},
				{
					"indexed": false,
					"internalType": "uint256",
					"name": "duration",
					"type": "uint256"
				}
			],
			"name": "TokensDeposited",
			"type": "event"
		},
		{
			"anonymous": false,
			"inputs": [
				{
					"indexed": true,
					"internalType": "address",
					"name": "purchaser",
					"type": "address"
				},
				{
					"indexed": true,
					"internalType": "address",
					"name": "beneficiary",
					"type": "address"
				},
				{
					"indexed": false,
					"internalType": "uint256",
					"name": "value",
					"type": "uint256"
				},
				{
					"indexed": false,
					"internalType": "uint256",
					"name": "amount",
					"type": "uint256"
				}
			],
			"name": "TokensPurchased",
			"type": "event"
		},
		{
			"anonymous": false,
			"inputs": [
				{
					"indexed": false,
					"internalType": "address",
					"name": "token",
					"type": "address"
				},
				{
					"indexed": false,
					"internalType": "uint256",
					"name": "amount",
					"type": "uint256"
				}
			],
			"name": "TokensReleased",
			"type": "event"
		},
		{
			"anonymous": false,
			"inputs": [
				{
					"indexed": false,
					"internalType": "address",
					"name": "receiver",
					"type": "address"
				},
				{
					"indexed": false,
					"internalType": "uint256",
					"name": "amount",
					"type": "uint256"
				}
			],
			"name": "TokensWithdrawn",
			"type": "event"
		},
		{
			"inputs": [],
			"name": "amyListingTime",
			"outputs": [
				{
					"internalType": "uint256",
					"name": "",
					"type": "uint256"
				}
			],
			"stateMutability": "view",
			"type": "function",
			"constant": true
		},
		{
			"inputs": [],
			"name": "internallyVested",
			"outputs": [
				{
					"internalType": "uint256",
					"name": "",
					"type": "uint256"
				}
			],
			"stateMutability": "view",
			"type": "function",
			"constant": true
		},
		{
			"inputs": [],
			"name": "owner",
			"outputs": [
				{
					"internalType": "address",
					"name": "",
					"type": "address"
				}
			],
			"stateMutability": "view",
			"type": "function",
			"constant": true
		},
		{
			"inputs": [
				{
					"internalType": "address",
					"name": "",
					"type": "address"
				},
				{
					"internalType": "uint256",
					"name": "",
					"type": "uint256"
				}
			],
			"name": "purchases",
			"outputs": [
				{
					"internalType": "uint256",
					"name": "purchased",
					"type": "uint256"
				},
				{
					"internalType": "uint256",
					"name": "released",
					"type": "uint256"
				},
				{
					"internalType": "uint256",
					"name": "lastReleaseTime",
					"type": "uint256"
				},
				{
					"internalType": "uint256",
					"name": "purchaseTime",
					"type": "uint256"
				},
				{
					"internalType": "uint256",
					"name": "duration",
					"type": "uint256"
				}
			],
			"stateMutability": "view",
			"type": "function",
			"constant": true
		},
		{
			"inputs": [],
			"name": "renounceOwnership",
			"outputs": [],
			"stateMutability": "nonpayable",
			"type": "function"
		},
		{
			"inputs": [
				{
					"internalType": "address",
					"name": "",
					"type": "address"
				}
			],
			"name": "revokedInvestors",
			"outputs": [
				{
					"internalType": "bool",
					"name": "",
					"type": "bool"
				}
			],
			"stateMutability": "view",
			"type": "function",
			"constant": true
		},
		{
			"inputs": [],
			"name": "totalVestingBalance",
			"outputs": [
				{
					"internalType": "uint256",
					"name": "",
					"type": "uint256"
				}
			],
			"stateMutability": "view",
			"type": "function",
			"constant": true
		},
		{
			"inputs": [
				{
					"internalType": "address",
					"name": "newOwner",
					"type": "address"
				}
			],
			"name": "transferOwnership",
			"outputs": [],
			"stateMutability": "nonpayable",
			"type": "function"
		},
		{
			"inputs": [],
			"name": "configInternalAllocations",
			"outputs": [],
			"stateMutability": "nonpayable",
			"type": "function"
		},
		{
			"inputs": [],
			"name": "initializeInternalVesting",
			"outputs": [],
			"stateMutability": "nonpayable",
			"type": "function"
		},
		{
			"inputs": [
				{
					"internalType": "address",
					"name": "beneficiary",
					"type": "address"
				},
				{
					"internalType": "uint256",
					"name": "amyAmount",
					"type": "uint256"
				}
			],
			"name": "internalDeposit",
			"outputs": [
				{
					"internalType": "bool",
					"name": "success",
					"type": "bool"
				}
			],
			"stateMutability": "nonpayable",
			"type": "function"
		},
		{
			"inputs": [
				{
					"internalType": "address",
					"name": "beneficiary",
					"type": "address"
				},
				{
					"internalType": "uint256",
					"name": "amyAmount",
					"type": "uint256"
				}
			],
			"name": "deposit",
			"outputs": [
				{
					"internalType": "bool",
					"name": "success",
					"type": "bool"
				}
			],
			"stateMutability": "nonpayable",
			"type": "function"
		},
		{
			"inputs": [],
			"name": "release",
			"outputs": [
				{
					"internalType": "bool",
					"name": "success",
					"type": "bool"
				}
			],
			"stateMutability": "nonpayable",
			"type": "function"
		},
		{
			"inputs": [
				{
					"internalType": "address",
					"name": "i",
					"type": "address"
				},
				{
					"internalType": "uint256",
					"name": "purchaseNumber",
					"type": "uint256"
				}
			],
			"name": "getReleasable",
			"outputs": [
				{
					"internalType": "uint256",
					"name": "",
					"type": "uint256"
				}
			],
			"stateMutability": "view",
			"type": "function",
			"constant": true
		},
		{
			"inputs": [
				{
					"internalType": "address",
					"name": "i",
					"type": "address"
				},
				{
					"internalType": "uint256",
					"name": "purchaseNumber",
					"type": "uint256"
				}
			],
			"name": "getVested",
			"outputs": [
				{
					"internalType": "uint256",
					"name": "",
					"type": "uint256"
				}
			],
			"stateMutability": "view",
			"type": "function",
			"constant": true
		},
		{
			"inputs": [
				{
					"internalType": "address",
					"name": "i",
					"type": "address"
				}
			],
			"name": "getPurchases",
			"outputs": [
				{
					"components": [
						{
							"internalType": "uint256",
							"name": "purchased",
							"type": "uint256"
						},
						{
							"internalType": "uint256",
							"name": "released",
							"type": "uint256"
						},
						{
							"internalType": "uint256",
							"name": "vested",
							"type": "uint256"
						},
						{
							"internalType": "uint256",
							"name": "releasable",
							"type": "uint256"
						},
						{
							"internalType": "uint256",
							"name": "lastReleaseTime",
							"type": "uint256"
						},
						{
							"internalType": "uint256",
							"name": "purchaseTime",
							"type": "uint256"
						},
						{
							"internalType": "uint256",
							"name": "timestamp",
							"type": "uint256"
						}
					],
					"internalType": "struct AMYVesting.EnhancedPurchase[]",
					"name": "",
					"type": "tuple[]"
				}
			],
			"stateMutability": "view",
			"type": "function",
			"constant": true
		},
		{
			"inputs": [
				{
					"internalType": "address",
					"name": "i",
					"type": "address"
				}
			],
			"name": "isRevoked",
			"outputs": [
				{
					"internalType": "bool",
					"name": "",
					"type": "bool"
				}
			],
			"stateMutability": "view",
			"type": "function",
			"constant": true
		},
		{
			"inputs": [],
			"name": "listingTime",
			"outputs": [
				{
					"internalType": "uint256",
					"name": "",
					"type": "uint256"
				}
			],
			"stateMutability": "view",
			"type": "function",
			"constant": true
		},
		{
			"inputs": [
				{
					"internalType": "address",
					"name": "i",
					"type": "address"
				}
			],
			"name": "revokeTokens",
			"outputs": [
				{
					"internalType": "bool",
					"name": "",
					"type": "bool"
				}
			],
			"stateMutability": "nonpayable",
			"type": "function"
		},
		{
			"inputs": [],
			"name": "getDuration",
			"outputs": [
				{
					"internalType": "uint256",
					"name": "",
					"type": "uint256"
				}
			],
			"stateMutability": "view",
			"type": "function",
			"constant": true
		},
		{
			"inputs": [
				{
					"internalType": "uint256",
					"name": "_duration",
					"type": "uint256"
				}
			],
			"name": "setDuration",
			"outputs": [],
			"stateMutability": "nonpayable",
			"type": "function"
		},
		{
			"inputs": [
				{
					"internalType": "uint256",
					"name": "lTime",
					"type": "uint256"
				}
			],
			"name": "setListingTime",
			"outputs": [],
			"stateMutability": "nonpayable",
			"type": "function"
		},
		{
			"inputs": [],
			"name": "finalize",
			"outputs": [],
			"stateMutability": "nonpayable",
			"type": "function"
		},
		{
			"inputs": [
				{
					"internalType": "address",
					"name": "depositer",
					"type": "address"
				}
			],
			"name": "setDepositer",
			"outputs": [],
			"stateMutability": "nonpayable",
			"type": "function"
		},
		{
			"inputs": [
				{
					"internalType": "address",
					"name": "depositer",
					"type": "address"
				}
			],
			"name": "isDepositer",
			"outputs": [
				{
					"internalType": "bool",
					"name": "",
					"type": "bool"
				}
			],
			"stateMutability": "view",
			"type": "function",
			"constant": true
		}
	]